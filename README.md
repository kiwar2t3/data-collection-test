# README #

Steps are necessary to get your application up and running.

### What is this repository for? ###

* Summary: Data-Collection-test is a data orientated service, specializing in web scrapping. The project is capable of iterating over entire website and extract information based on the model. We 
need a model to extract information, since there is no standards how the information is linked. For the sake of testing I setup the model for popular website "walmart.ca". 
Given a root url and a model (html tags names and its to extract information) this application can iterate over entire website and collect product names, details, price and put them in an excel file. 
Although it could have used java.net library to open the URL and jSOUP to read the html tags, but many of the website doesn't allow connections that are not called from the browsers itself. 

* To avoid getting refused from the server, it uses Selenium web-driver (Firefox), this will make a call to the url by opening the Firefox page to mimic user call. 
Once getting the Url data as string everything else is pretty much parsing and saving strings. For each category the application will create an excel file 
with the related product data information in it. It is ready to build and run on windows machine, running on MAC would only take enabling one 
line in SeleniumMain.java (I had a class that will take care of all the OS types, but to simplify I removed it)

* Version 0.1

### How do I get set up? ###


1. Project use maven to build its dependencies from pom.xml file. it is strongly recommended to use intellij to run this application.
2. It uses binary file of geckodriver to call the Firefox browser. 

* JRE: 1.8
* selenium: 3.0.1
* poi: 3.11

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Ali