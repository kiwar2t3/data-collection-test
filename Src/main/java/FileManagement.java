import enums.ops;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.List;

/**
 * Created by Mohammed on 1/11/2017.
 */
public class FileManagement {

    private static String BasePath;
    private static String DataFolder = "\\src\\DataFiles\\";
    private static String BaseFileName = "null";

    FileInputStream Fis = null;
    FileOutputStream Fos = null;
    File MainFile = null;


    public void DeleteFile(String fileName){

    }

    //open a file if not exist create one
    public void FileWrite(String fileName, List<String> baseElemnts)throws Exception{
      //  Writer writer = null;
        /*try{
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(BasePath+DataFolder+fileName),"utf-8"
            ));*/
            File file = new File(BasePath+DataFolder+fileName);
            if(!file.exists()){
                file.createNewFile();
            }
        /*}catch (IOException ex){
            ex.printStackTrace();
        }finally {
            writer.close();
        }*/
    }


    /*public void CreateFile(String fileName){
        OpenClose(ops.open);
        OpenClose(ops.write);
    }*/

    public void OpenClose(ops fileOps, String fileName) throws Exception{

        if(BasePath == null)
            SetBasePath();

        if(fileName != null)
            BuildFileName(fileName);

        if(BaseFileName == null)
                throw new Exception("File name cannot be null");

        MainFile = new File(BasePath+DataFolder+BaseFileName);

        switch (fileOps){
            case open:

                try{
                    if(!MainFile.exists())
                        MainFile.createNewFile();

                 Fos = new FileOutputStream(MainFile.getPath());
                }catch (IOException ex){
                    ex.printStackTrace();
                }
                break;
            case close:
                try {
                    Fos.close();
                    BaseFileName ="";
                }catch (IOException ex){}
                break;
        }
    }

    public FileInputStream FileInStream() throws Exception{
       return new FileInputStream(BasePath+DataFolder+BaseFileName);
    }

    public void FileReadWrite(XSSFWorkbook currWorkBook) throws Exception {

            if(BaseFileName == null)
                System.out.println("Please set the file name");

            if(Fos == null)
                OpenClose(ops.open, BaseFileName);

            currWorkBook.write(Fos);
            Fos.flush();

    }

    private void BuildFileName(String fileName){
            BaseFileName =  fileName.replaceAll("[^\\w\\s]","")+".xlsx";
    }

    private String SetBasePath(){
        BasePath = System.getProperty("user.dir");
        return BasePath;
    }

    public String GetBasePaht(){
        if(BasePath == null)
            return SetBasePath();
        return BasePath;
    }

}
