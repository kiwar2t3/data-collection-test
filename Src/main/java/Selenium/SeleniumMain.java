package Selenium;

import DataObjects.Item;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import sun.awt.OSInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mohammed on 1/12/2017.
 */
public class SeleniumMain {

    public SeleniumMain(String basePath){

        //set the system property to use Gecko drive to use firefox for windows
        System.setProperty("webdriver.gecko.driver", basePath +"\\src\\Support"+"\\geckodriver.exe");

        //set the system property to use Gecko drive to use firefox for Mac
        //System.setProperty("webdriver.gecko.driver", basePath +"/src/Support/Mac"+"/geckodriver");

    }

    WebDriver WDriver = null;
    public String PaginationBaseUrl;

    public void CloseDriver(){
        WDriver.quit();
    }


    /*public Map<String, String> GetElements(String url, String catStr, boolean getDataFlag){

        PaginationBaseUrl= url;
        FetchNavTreeElements(url,catStr);

        if(getDataFlag){
            FetchProductsData();
            return null;
        }

        if(getDataFlag)
            FetchData();

        return elements;
    }*/

    public Map<String, String> FetchNavTreeElements(String url, String catStr){
        Map<String, String> navUrlData = new HashMap<String, String>();
        try{
            for (WebElement wb:CallSel(url,catStr).findElements(By.tagName("li"))) {
                WebElement subElement =  wb.findElement(By.tagName("a"));
                navUrlData.put(subElement.findElement(By.tagName("span")).getText(),
                        subElement.getAttribute("href"));
          /*  int i = 0;
            /*for (WebElement wb:CallSel(url,catStr).findElements(By.tagName("li"))) {
                WebElement subElement =  wb.findElement(By.tagName("a"));
                navUrlData.put(subElement.findElement(By.tagName("span")).getText(),
                        subElement.getAttribute("href"));*/
          /*  int i = 0;
            for (WebElement wb:CallSel(url,catStr).findElements(By.tagName("li"))) {

                WebElement subElement =  wb.findElement(By.tagName("a"));
                String key = subElement.findElement(By.tagName("span")).getText();
                navUrlData.put(key == ""? key: "Key"+i++,
                        subElement.getAttribute("href"));*/

        }}catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return navUrlData;
    }

    public Map<Item, String> FetchProductsData(String url, String catStr){
        PaginationBaseUrl= url;
        Map<Item, String> items = new HashMap<Item, String>();
        boolean done = true;
        CallSel(url, catStr);
        //WebElement we = WDriver.findElement(By.className("page-select"));
        IterateElemets(items);
        List<String> pagenationUrls = new ArrayList<String>();
        int paginationCount = 0;


       // JavascriptExecutor js = ((JavascriptExecutor) WDriver);
       // js.executeScript("window.scrollTo(0, document.body.scrollHeight)");


        try {
            paginationCount = WDriver.findElement(By.id("shelf-pagination"))
                    .findElement(By.className("page-select"))
                    .findElements(By.tagName("option")).size();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        if(paginationCount > 0){
            for(int i =2;i<paginationCount+1; i++)
                pagenationUrls.add(BuildPageUrl(null,i));
        }

        //iterate over pagination
        if(pagenationUrls.size() > 0){
            for (String s: pagenationUrls
                 ) {
                CallSel(s, catStr);
                IterateElemets(items);
            }
        }

        return items;
    }

    public Map<Item, String> FetchHProductsData(String url, String catStr){
        PaginationBaseUrl= url;
        Map<Item, String> items = new HashMap<Item, String>();
        boolean done = true;
        CallSel(url, catStr);
        IterateElemets(items);
       /* List<String> pagenationUrls = new ArrayList<String>();
        int paginationCount = 0;
        try {
            paginationCount = WDriver.findElement(By.id("shelf-pagination"))
                    .findElement(By.className("page-select"))
                    .findElements(By.tagName("option")).size();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        if(paginationCount > 0){
            for(int i =2;i<paginationCount+1; i++)
                pagenationUrls.add(BuildPageUrl(null,i));
        }

        //iterate over pagination
        if(pagenationUrls.size() > 0){
            for (String s: pagenationUrls
                    ) {
                CallSel(s, catStr);
                IterateElemets(items);
            }
        }*/

        return items;
    }

    //create a url to iteratae over pages.
    public String BuildPageUrl(String seed, int i){
        return PaginationBaseUrl +"/page-"+i;
    }


    //iterate over elements to fetch desired data
    //Remember this model for elements is not standard, each
    //website will have their personal standards for elements hierarchy, you will
    //need to build the element hierarchy by yourself or you will have to
    //write machine learning algorithm to do it for you
    private void IterateElemets(Map<Item, String> items){
        int i =0;
        try{
       // for (WebElement wE: ((FirefoxDriver) WDriver).findElementById("shelf-page").findElements(By.tagName("article"))) {
        for (WebElement wE: ((FirefoxDriver) WDriver).findElementById("shelf-page").findElements(By.tagName("article"))) {
            Item prodcut = Item.Make(
                    wE.findElement(By.className("details")).findElement(By.className("title")).getText(),
                    wE.findElement(By.className("details")).findElement(By.className("description")).getText(),
                    wE.findElement(By.className("rating")).findElement(By.tagName("span")).getAttribute("class"),
                    wE.findElement(By.className("price-current")).findElement(By.className("product-price-analytics")).getAttribute("data-analytics-value"),
                    "","");

           /* for (WebElement wE:  WDriver.findElement(By.className("products-grid")).findElements(By.className("item"))){
                Item prodcut = Item.Make(wE.findElement(By.className("product-name")).getText()
                        ,""
                        ,""
                        ,wE.findElement(By.className("price-box")).findElement(By.tagName("span")).getText()
                        ,"",
                        PaginationBaseUrl);*/
            items.put(prodcut, i+++"");
        }}catch (Exception ex){
            System.out.println(ex.fillInStackTrace());
        }
    }


    //initiate the browser
    public WebElement CallSel(String url, String catStr){

        //set firefox driver path

        DesiredCapabilities capabilities= DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", true);

        // Create a new instance of the Firefox driver
        // Notice that the remainder of the code relies on the interface,
        // not the implementation.
        if(WDriver == null)
            WDriver = new FirefoxDriver(capabilities);

        // And now use this to visit desired site
        WDriver.get(url);

        //scroll up down the window.
        JavascriptExecutor jse = (JavascriptExecutor)WDriver;
        jse.executeScript("window.scrollBy(0,250)", "");
        jse.executeScript("window.scrollBy(350,0)", "");

        // Find the text input element by its name
        return WDriver.findElement(By.className(catStr));
    }
}
