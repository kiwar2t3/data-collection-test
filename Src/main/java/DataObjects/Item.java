package DataObjects;

import javax.xml.bind.annotation.*;

/**
 * Created by Mohammed on 1/13/2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Product")
public class Item {

    public Item(){}

    public Item(String productName,
                String imageUrl,
                String productRating,
                String price,
                String productDetails,
                String category){
        ProductName = productName;
        ImageUrl = imageUrl;
        ProductRating = productRating;
        Price = price;
        ProductDetails = productDetails;
        Category = category;

    }

    public static Item Make(String productName,
                            String imageUrl,
                            String productRating,
                            String price,
                            String productDetails,
                            String category){

        return new Item(productName, imageUrl, productRating, price, productDetails, category);
    }

    private String ProductName;

    private String Category;

    private String ImageUrl;

    private String ProductRating;

    private String Price;

    private String ProductDetails;

    public String getProductCategory()
    {
        return ProductName;
    }

    public void setProductCategory(String productCategory)
    {
        this.Category = productCategory;
    }

    public String getProductName()
    {
        return ProductName;
    }

    public void setProductName(String productName)
    {
        this.ProductName = productName;
    }

    public String getImageUrl()
    {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.ImageUrl = imageUrl;
    }

    public String getProductRating()
    {
        return ProductRating;
    }

    public void setProductRating(String productRating)
    {
        this.ProductRating = productRating;
    }

    public String getPrice()
    {
        return Price;
    }

    public void setPrice(String price)
    {
        this.Price = price;
    }

    public String getProductDetails()
    {
        return ProductDetails;
    }

    public void setProductDetails(String productDetails)
    {
        this.ProductDetails = productDetails;
    }

    @Override
    public String toString()
    {
        return "ProdTest [ProductName = "+ ProductName +",  ProductDetails = "+ ProductDetails +",ProductRating = "+ ProductRating +",  Price = "+ Price +",ImageUrl = "+ ImageUrl +"]";
    }
}
