
/**
 * Created by Mohammed on 1/11/2017.
 */

import DataObjects.Item;
import Selenium.SeleniumMain;
import enums.ops;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.Map;


public class MainClass  {

    public static void main(String[] args) throws Exception {
        FileManagement fm = new FileManagement();
        ExcelManagement em = new ExcelManagement(null);
        SeleniumMain seleniumMain = new SeleniumMain(fm.GetBasePaht());
        Map<String, String> elements = null;

     /*   ///////TESTING CODE
        MainClass c = new MainClass();
        c.Test(em.CurrWorkBook, "Sheet 1");
        fm.FileReadWrite(em.CurrWorkBook);
        em.ReStartWorkSheet(fm.FileInStream());
        c.Test(em.CurrWorkBook, "Sheet 2");
        fm.FileReadWrite(em.CurrWorkBook);
        c.Test(em.CurrWorkBook, "Sheet 3");
        fm.FileReadWrite(em.CurrWorkBook);
        fm.OpenClose(ops.close);
        fm.OpenClose(ops.open, "hfksfh-sdkjf-df3#");
        em.CreateSheetHeader("Main->Data->","Test");
        fm.FileReadWrite(em.CurrWorkBook);
        fm.OpenClose(ops.close, null);
        ///////////////*/


        int i =0;
        elements = seleniumMain.FetchNavTreeElements("http://www.walmart.ca/en","category-tree");
        em.WriteToExcel("Main", elements);

        //write to a file
         for (Map.Entry<String,String>  subElements :elements.entrySet()) {
             Map<String, String> gSubElementsData = seleniumMain.FetchNavTreeElements(subElements.getValue(), "selected");
            //em.WriteToExcel("Main->Cat", subElements);

             for (Map.Entry<String,String> subS: gSubElementsData.entrySet()) {
                // String temp = "http://www.walmart.ca/en/outdoor-living/lawn-and-garden/N-2075";
                Map<String, String> gSubElements = seleniumMain.FetchNavTreeElements(subS.getValue(),"selected");
                // Map<String, String> gSubElements = seleniumMain.FetchNavTreeElements(temp,"selected");
                //em.WriteToExcel("Main->spreads-syrups", gSubElements);
                boolean newCatogeryFlag = true;
                for (Map.Entry<String,String> ssubS:gSubElements.entrySet()) {

                    //open the file
                    if(newCatogeryFlag){
                        fm.OpenClose(ops.open, ssubS.getValue());
                        newCatogeryFlag = false;
                    }

                    //get the elemets that are interested to you
                    Map<Item, String> prodElementsData = seleniumMain.FetchProductsData(ssubS.getValue(),"selected");

                    //handle excel read write the data
                    em.CreateSheetHeader("Main->"+"->"+"Data->"+ssubS.getKey(),ssubS.getKey()+i);
                    em.WriteToExcel(prodElementsData, ssubS.getKey()+i);

                    //do not overload the request to the server
                    Thread.sleep(200000+(int)Math.random());
                    i++;
                  /*  newCatogeryFlag = true;
                    //write to a file and close the file
                    fm.FileReadWrite(em.CurrWorkBook);
                    fm.OpenClose(ops.close, null);*/

                }
                 newCatogeryFlag = true;
                 //write to a file and close the file
                 fm.FileReadWrite(em.CurrWorkBook);
                 fm.OpenClose(ops.close, null);
                 em = new ExcelManagement(null);

            }
        }

        /*elements = seleniumMain.FetchNavTreeElements("http://nj.hmart.com/shop/ready-to-serve/","level0");

                    em.CreateSheetHeader("Main->"+s.getKey()+"->"+subS.getKey()+"->"+"Data->"+ssubS.getKey(),ssubS.getKey()+i);
                    em.WriteToExcel(prodElementsData, ssubS.getKey()+i);
                    Thread.sleep(1000+(int)Math.random());
                    i++;
                }
                fm.FileReadWrite(em.CurrWorkBook);
            }
        }*/

        //Close the browser
        seleniumMain.CloseDriver();
    }

    public void Test(XSSFWorkbook CurrWorkBook, String sheetName){
        XSSFSheet curSheet = CurrWorkBook.getSheet(sheetName);
        if(curSheet == null)
            curSheet = CurrWorkBook.createSheet(sheetName);


            Row row =  curSheet.createRow(0);
            Cell cell0 = row.createCell(0);
            cell0.setCellValue("Key");

            Cell cell1 = row.createCell(1);
            cell1.setCellValue("Test sheet"+sheetName);
    }
}