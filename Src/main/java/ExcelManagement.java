import DataObjects.Item;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed on 1/12/2017.
 */
public class ExcelManagement {
    public XSSFWorkbook CurrWorkBook = null;

    public ExcelManagement(FileInputStream fileInStream){
        ReStartWorkSheet(fileInStream);

    }

    public void ReStartWorkSheet(FileInputStream fileInStream){
       try {
           if(fileInStream == null)
               CurrWorkBook = new XSSFWorkbook();
           else
               CurrWorkBook = new XSSFWorkbook(fileInStream);
       }catch (IOException ex){}

    }

    public void ReadFrmExcel(){

    }

    public void CreateSheetHeader(String header, String sheetName){
        XSSFSheet curSheet = CurrWorkBook.getSheet(sheetName);
        if(curSheet == null)
            curSheet = CurrWorkBook.createSheet(sheetName);

        Row row =  curSheet.createRow(0);

        Cell cell0 = row.createCell(0);
        cell0.setCellValue(header);

    }

    public Row GetRow(String sheet, int row) throws Exception {
            return GetSheet(sheet).getRow(row);
    }

    public XSSFSheet GetSheet(String sheetName) throws Exception {

        if(CurrWorkBook == null)
            throw new Exception("File not set");

        XSSFSheet sh = CurrWorkBook.getSheet(sheetName);

        if (sh == null)
            return CurrWorkBook.createSheet(sheetName);
        return sh;
    }

    public Map<String,String> GetSheeElements(String sheetName)throws Exception{
        Map<String,String> sheetElements = new HashMap<String, String>();
        for (Row r: GetSheet(sheetName)){
            sheetElements.put(r.getCell(0).getStringCellValue()
                    ,  r.getCell(1).getStringCellValue());
        }
        return sheetElements;
    }

    public void WriteToExcel(String sheetName, Map<String, String> elements){
        XSSFSheet curSheet = CurrWorkBook.getSheet(sheetName);
        if(curSheet == null)
            curSheet = CurrWorkBook.createSheet(sheetName);

        int i = 0;
        for (Map.Entry<String, String> e: elements.entrySet()) {

            Row row =  curSheet.createRow(i++);
            Cell cell0 = row.createCell(0);
            cell0.setCellValue(e.getKey());

            Cell cell1 = row.createCell(1);
            cell1.setCellValue(e.getValue());
        }

    }

    public void  WriteToExcel(Map<Item, String> products, String sheetName){
        XSSFSheet curSheet = CurrWorkBook.getSheet(sheetName);
        //if(curSheet == null)
          //  curSheet = CurrWorkBook.createSheet(sheetName);

        int i = 1;
        for (Map.Entry<Item,String> e: products.entrySet()) {

            Row row =  curSheet.createRow(i++);

            Cell cell0 = row.createCell(0);
            cell0.setCellValue(e.getKey().getProductName());

            Cell cell1 = row.createCell(1);
            cell1.setCellValue(e.getKey().getProductDetails());

            Cell cell2 = row.createCell(2);
            cell2.setCellValue(e.getKey().getPrice());

            Cell cell3 = row.createCell(3);
            cell3.setCellValue(e.getKey().getProductRating());
        }

    }
}
